# Ubuntu 15.04 with Java 8 installed.

FROM ubuntu:bionic
MAINTAINER Suman Kanrar, https://github.com/SumanKanrar-IEM
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install wget -y && \
    apt-get install openjdk-8-jdk -y && \
    apt-get install unzip -y && \
    apt-get install maven -y && \
    apt-get install git -y && \
    apt-get update

RUN mvn --version
RUN java -version
RUN apt-get install nodejs -y && \
    apt-get install npm -y
